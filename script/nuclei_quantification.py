import argparse
import os
import re

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

from timagetk.io.image import read_lsm_image
from timagetk.algorithms.peak_detection import detect_nuclei
from timagetk.algorithms.signal_quantification import quantify_nuclei_signal_intensity

default_dirname = "../data"


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', help='Path to the directory containing images', default=default_dirname)
    parser.add_argument('-c', '--channel-names', default=['MB', 'MV', 'Visible'], nargs='+', help='List of names for the image channels')
    args = parser.parse_args()

    dirname = args.directory
    assert os.path.exists(dirname)
    filenames = [os.path.splitext(f)[0] for f in os.listdir(dirname) if f.endswith(".lsm")]

    channel_names = args.channel_names

    ratio_range = (0, 3)
    percentile_range = (10, 90)

    cell_datas = []

    for filename in filenames:
        print("--> Processing",filename)

        image_filename = dirname + "/" + filename + ".lsm"

        if 'dark' in filename:
            radius_range = (1., 4.5) # leaf
            #radius_range = (2.0, 4.5) # hypocotyle
        else:
            radius_range = (1.2, 5.0) # leaf
            # radius_range = (2.5, 5.0) # hypocotyle
        step = 0.3 # leaf
        # step = 0.2  # hypocotyle

        img_dict = read_lsm_image(
            image_filename,
            channel_names=channel_names,
            return_order='ZYX'
        )

        if img_dict['MB'].dtype == np.uint8:
            threshold = 12
            signal_max = 255 # leaf
            # signal_max = 128 # hypocotyle
        else:
            threshold = 3000
            signal_max = 20000

        nuclei_points = detect_nuclei(
            img_dict['MB'],
            radius_range=radius_range,
            threshold=threshold,
            step=step
        )

        cell_data = pd.DataFrame({'label': range(len(nuclei_points))})
        for k, dim in enumerate('xyz'):
            cell_data[f'center_{dim}'] = nuclei_points[:, k]

        for channel_name in ['MB', 'MV']:
            cell_data[channel_name] = quantify_nuclei_signal_intensity(
                img_dict[channel_name],
                nuclei_points,
                nuclei_sigma=0.1
            )
        cell_data['MV/MB'] = cell_data['MV']/cell_data['MB']

        cell_data['t'] = 'dark' if 'dark' in filename else 'light'
        hip_id = int(re.findall('_hip[0-9]+',filename)[0][4:]) if 'hip' in filename else int(re.findall('_h[0-9]+',filename)[0][2:])
        cell_data['h'] = hip_id
        cell_data['zone'] = re.findall('_zone[A-Z]+',filename)[0][5:] if 'zone' in filename else ''
        cell_data['c_treatment'] = re.findall('_treat[A-Z]+',filename)[0][6:] if 'treat' in filename else ''
        experiment_match = re.findall('e[0-9]+',filename)
        cell_data['e'] = int(experiment_match[0][1:]) if len(experiment_match)>0 else ''
        cell_data['GA'] = 3 - cell_data['MV/MB']

        cell_data.to_csv(dirname + "/" + filename + "_cells.csv",index=False)

        cell_datas += [cell_data]

        extent = (
            (-1/2)*img_dict['MB'].voxelsize[2], (img_dict['MB'].shape[2]-1/2)*img_dict['MB'].voxelsize[2],
            (img_dict['MB'].shape[2]-1/2)*img_dict['MB'].voxelsize[1], (-1/2)*img_dict['MB'].voxelsize[1]
        )

        figure = plt.figure(figsize=(12, 18))
        axes = figure.subplots(3, 2)

        ax = axes[0, 0]
        ax.imshow(img_dict['MB'].max(axis=0), cmap='Blues', vmin=0, vmax=signal_max, extent=extent)
        for x, y, l in cell_data[['center_x', 'center_y', 'label']].values:
            ax.text(x, y, f" {int(l)}", color='r', ha='left', va='bottom', size=8)
        ax.scatter(cell_data['center_x'], cell_data['center_y'], s=20, facecolor='none', edgecolor='r')
        ax.set_title("Nuclei Detection")

        ax = axes[0, 1]
        ax.imshow(img_dict['MB'].max(axis=0), extent=extent,alpha=0)
        col = ax.scatter(cell_data['center_x'], cell_data['center_y'], c=cell_data['MB'], s=20, cmap='Blues', vmin=0, vmax=signal_max)
        ax.set_title("Quantified MB")

        cax = ax.inset_axes([0.955, 0.015, 0.03, 0.25])
        cbar = figure.colorbar(col, cax=cax, pad=0.)
        cax.yaxis.set_ticks_position('left')
        cax.set_ylim(0, signal_max)

        ax = axes[1, 0]
        ax.imshow(img_dict['MV'].max(axis=0), extent=extent,alpha=0)
        col = ax.scatter(cell_data['center_x'], cell_data['center_y'], c=cell_data['MV'], s=20, cmap='Oranges', vmin=0, vmax=signal_max)
        ax.set_title("Quantified MV")

        cax = ax.inset_axes([0.955, 0.015, 0.03, 0.25])
        cbar = figure.colorbar(col, cax=cax, pad=0.)
        cax.yaxis.set_ticks_position('left')
        cax.set_ylim(0, signal_max)

        ax = axes[1, 1]
        ax.imshow(img_dict['MB'].max(axis=0), extent=extent,alpha=0)
        col = ax.scatter(cell_data['center_x'], cell_data['center_y'], c=cell_data['MV/MB'], ec='k', s=40, cmap='jet', vmin=ratio_range[0], vmax=ratio_range[1])
        ax.set_title("MV/MB Ratio (fixed range)")

        cax = ax.inset_axes([0.955, 0.015, 0.03, 0.25])
        cbar = figure.colorbar(col, cax=cax, pad=0.)
        cax.yaxis.set_ticks_position('left')
        cax.set_ylim(*ratio_range)

        ax = axes[2, 0]
        ax.imshow(img_dict['MB'].max(axis=0), extent=extent,alpha=0)
        col = ax.scatter(cell_data['center_x'], cell_data['center_y'], c=cell_data['MV/MB'], ec='k', s=40, cmap='jet', vmin=np.percentile(cell_data['MV/MB'], percentile_range[0]), vmax=np.percentile(cell_data['MV/MB'], percentile_range[1]))
        ax.set_title("MV/MB Ratio (own range)")

        cax = ax.inset_axes([0.955, 0.015, 0.03, 0.25])
        cbar = figure.colorbar(col, cax=cax, pad=0.)
        cax.yaxis.set_ticks_position('left')
        cax.set_ylim(np.percentile(cell_data['MV/MB'], percentile_range[0]), np.percentile(cell_data['MV/MB'], percentile_range[1]))

        ax = axes[2, 1]
        ax.boxplot(
            cell_data['MV/MB'],
            positions=[0], widths=[0.166],
            medianprops={'color': 'k', 'linewidth': 2},
            flierprops={'markersize': 5, 'alpha': 0.33},
            patch_artist=True, boxprops={'facecolor': 'b', 'edgecolor': 'k'}
        )
        ax.set_xlim(-1, 1)
        ax.set_xticks([])
        ax.set_ylim(*ratio_range)
        ax.set_ylabel("MV/MB Ratio")

        ax.text(0.5, ratio_range[0]+np.diff(ratio_range)/2, f"Max = {np.round(np.max(cell_data['MV/MB']),3)}", ha='left')
        ax.text(0.5, ratio_range[0]+3*np.diff(ratio_range)/8, f"P{percentile_range[1]} = {np.round(np.percentile(cell_data['MV/MB'],percentile_range[1]),3)}", ha='left')
        ax.text(0.5, ratio_range[0]+np.diff(ratio_range)/4, f"P{percentile_range[0]} = {np.round(np.percentile(cell_data['MV/MB'],percentile_range[0]),3)}", ha='left')
        ax.text(0.5, ratio_range[0]+np.diff(ratio_range)/8, f"Min = {np.round(np.min(cell_data['MV/MB']),3)}", ha='left')

        figure.tight_layout()
        figure.savefig(f"{dirname}/{filename}_quantification.png")

    all_cell_data = pd.concat(cell_datas)
    all_cell_data.to_csv(f"{dirname}/all_python.csv")
