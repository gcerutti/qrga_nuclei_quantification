# qRGA quantification script for 3D nuclei images

A Python script to perform nuclei detection and ratiometric signal quantification on 3D microscopy images.

## Author:
* Guillaume Cerutti (<guillaume.cerutti@ens-lyon.fr>)

## Contributors:
* Amelia Felipo Benavent (<amelia.felipo-benavent@ibmp-cnrs.unistra.fr>)
* Carlos Galvan Ampudia (<carlos.galvan-ampudia@ens-lyon.fr>)


## Installation

### Pre-requisite : Install conda

* Open a terminal window and type `conda`. If no error message appear (but a long how-to message) then you have successfully installed `conda`.

* Otherwise, you may choose to install either [the miniconda tool](https://docs.conda.io/en/latest/miniconda.html) or [the anaconda distribution](https://docs.anaconda.com/anaconda/install/) suitable for your OS.

### Download the source repository

#### Using the `git` command line tool

* Open a terminal window and navigate to the directory of your choice using the `cd` command.

* Copy the source repository using `git` by typing:

```
git clone https://gitlab.inria.fr/gcerutti/qrga_nuclei_quantification.git
```

#### Using the download link on Gitlab

* Click on the **Download** link (left to the **Clone** button) on top of this page to download the sources as an archive.

* Decompress the archive in the directory of your choice.

### Create a new conda environment

* In the terminal window, go to the directory where you copied the source repository:

```
cd qrga_nuclei_quantification
```

* Create a new environment containing all the script dependencies using the provided YAML file:

```
conda env create -f environment.yml
```

## Usage

### Activate the conda environment

* Each time you open a nw terminal window to use the script, you will need to activate the environment you created to access the dependencies

```
conda activate nuclei_quantification
```

### Run the quantification script

* Go the script directory within the directory where you have copied the source repository

```
cd path/to/qrga_nuclei_quantification/script
```

> **NOTE:** you need to replace `path/to` with the actual file path in your file system 

* To test that the script works correctly you may simply run:

```
python nuclei_quantification.py
```

* Run the quantification script on your own data by setting the correct arguments

```
python nuclei_quantification.py -d path/to/images/ -c MB MV
```

* `-d` should be a path to a direcctory containing LSM images [default : `../data/`]
* `-c` should be a list of channel names (separated by spaces) [default : `MB MV Visible PI`] among which there must be:
  * `MB`: the channel corresponding to the reference TagBFP marker
  * `MV`: the channel corresponding to the VENUS marked GA sensor

> **NOTE:** you need to replace `path/to` with the actual file path in your file system 


